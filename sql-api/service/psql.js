const { spawnSync } = require('child_process');

module.exports = function (req, res) {
  const response = {
    message: "",
    status: ""
  }
  const cmd = spawnSync('psql', ['-c', req.body.query]);
  if (cmd.error) {
    response.message = cmd.error.code;
    response.status = 'fatal';
  }
  else {
    if (cmd.stderr.toString() !== '') {
      response.message = cmd.stderr.toString();
      response.status = 'error';
    }
    else if (cmd.stdout.toString() !== '') {
      response.message = cmd.stdout.toString();
      response.status = 'success';
    }
    else {
      response.status = 'void';
    }
  }
  res.json(response);
};