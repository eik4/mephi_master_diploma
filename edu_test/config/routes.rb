Rails.application.routes.draw do
  resources :question_types, only: :index
  resources :sections
  resources :themes, except: :index

  resources :scenarios do
    member do
      get :test_sessions
    end
  end

  resources :questions, except: :index do
    collection do
      get 'add_answer'
    end
  end

  resources :test_sessions, only: [:show, :index, :new, :create] do
    collection do
      post 'pass'
      post 'pass_part'
    end
  end

  resources :users, only: [:index] do
    collection do
      get :edit_current_user
      patch :update_current_user
      get :reset_current_user
    end
    member do
      get :test_sessions
    end
  end

  namespace :api do
    resources :users
  end

  get 'console', to: 'console#index'

  root 'application#index'
  get '*any', to: 'application#index'
end
