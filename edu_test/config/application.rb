require_relative 'boot'

require 'rails/all'
require 'rack-cas/session_store/active_record'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module EduTest
  class Application < Rails::Application
    config.i18n.default_locale = :ru
    ActiveRecord::Base.belongs_to_required_by_default = true
    config.eager_load_paths << Rails.root.join("lib")
    config.eager_load_paths << Rails.root.join("lib", "extras")

    config.action_controller.permit_all_parameters = true
    config.rack_cas.session_store = RackCAS::ActiveRecordStore
    config.rack_cas.server_url='https://auth.mephi.ru'
    config.rack_cas.exclude_path='/api'

    config.generators do |g|
      g.stylesheets     false
      g.javascripts     false
    end
    config.time_zone = 'Moscow'
  end
end
