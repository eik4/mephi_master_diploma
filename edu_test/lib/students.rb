class Students
  include HTTParty
  base_uri 'https://students.mephi.ru'

  def self.groups_count(year, term, date = nil)
    hash = {login: 'roganov', password: 'skei568739TY', term: term, year: year}
    if date
      hash['current_date'] = date
    end
    buf = JSON.parse get("/students_api/groups_quantity.json", query: hash).body
  rescue
    []
  end

  def self.group_info(year, term, group_name)
    hash = {login: 'roganov', password: 'skei568739TY', term: term, year: year, group_name: group_name}
    buf = JSON.parse get("/students_api/by_term.json", query: hash).body
  rescue
    []
  end
end
