class Corporate
  include HTTParty

  base_uri 'https://schedule.mephi.ru'
  digest_auth 'roganov', 'roganov_api'

  def self.groups(code)
    JSON.parse get('/api/groups.json', query: {code: code}).body
  rescue Exception => e
    {error: e.message}
  end

end
