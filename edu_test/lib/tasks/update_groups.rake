require 'corporate'

desc 'Обновить список студенческих групп'
task :update_groups => :environment do

  Corporate.groups(20171).each do |g|
    Group.create(name: g["display_name"])
  end
  Group::GROUPS.each do |name|
    Group.create(name: name)
  end
  
end
