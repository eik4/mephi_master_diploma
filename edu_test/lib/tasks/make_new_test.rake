# Модифицировать чуть-чуть сценарий, изменив кол-во вопросов и время
task :make_new_test => :environment do
  s_old = Scenario.find(5)
  sc_old = s_old.scenario_themes.first
  
  sc = Scenario.create(
    name: s_old["name"] + " - контрольный",
    duration: 15
  )
  ScenarioTheme.create(
    scenario: sc,
    theme: sc_old.theme,
    questions: 7
  )
end

# Обновить сценарий, сохраняя все его параметры, но взяв обновлённые
# (исправленные) веррсии входящих в него тем

task :update_scenario, [:name] => :environment do |t, args|
  scenario = Scenario.find_by_name(args[:name])
  raise "Can't find Scenario #{args[:name]}" if scenario.nil?

  sc = Scenario.create(
    name: scenario.name,
    duration: scenario.duration
  )
  raise "Can't create Scenario #{scenario.name}" if sc.nil?

  scenario.scenario_themes.each do |sc_th|
    th = sc_th.theme
    section = Section.where(name: th.section.name, version: nil).first
    theme = Theme.where(name: th.name, section: section).first
    ScenarioTheme.create(
      scenario: sc,
      theme: theme,
      questions: sc_th.questions
    )
  end
end
