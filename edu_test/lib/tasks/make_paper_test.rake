def pr_qw(array, with_answer=false)
  q = array[@i%array.size]
  #at = q.answer.body
  result = "\\noindent\n**Задание #{@i}.** #{q.body}\n\n"
  #raise [q, at].inspect
  result += "\\vspace*{4mm}" if q.answer_variants.count != 0
  q.answer_variants.shuffle.each do |a|
    result += '\noindent\scriptsize\raisebox{1mm}{\fbox{\phantom{o}}}\vspace*{-8mm}\normalsize\qquad'
    result += "\n\n#{AnswerVariant.find(a).body.sub(/isinstance/, '   isinstance')}\n\n"
  end
  unless with_answer
    if q.answer_variants.count == 0
      result += "\n\n\\noindent\\rule{3cm}{0.2mm}\n\n"
    end
    return result
  end
  
  result += "\n\n\\hrule\n\n"
  q.answer.body.each do |a|
    if a.class == String
      result += "~~~\n"
      result += "#{a}\n"
      result += "~~~\n\n"
    else
      result += "#{AnswerVariant.find(a).body}\n"
    end
  end
  result += "\n\n"
end



# Подготовить печатный вариант теста
#
# Специализировано только для одного конкретного тестирования!
#
task :make_paper_test => :environment do

  # Тема (ы)
  theme_id = 35

  questions = Theme.find(theme_id).questions
  q_min_max_sum, other = questions.partition{|q| q.body =~ /sum/ && q.body =~/min/ && q.body =~ /max/}
  q_len, other = other.partition{|q| q.body =~ /len/}
  q_any, other = other.partition{|q| q.body =~ /any/}
  q_all, q_isinstance = other.partition{|q| q.body =~ /all/}

  # Количество вариантов
  quantity = 10

  Dir.chdir('/tmp') do
    File.open('test.md', 'w') do |f|
      File.open('test_with_answers.md', 'w') do |fa|

        f.puts "\\newpage\n\n"
        fa.puts "\\newpage\n\n"
        quantity.times do |var|

          # Перемешиваем вопросы
          q_min_max_sum.shuffle!
          q_len.shuffle!
          q_any.shuffle!
          q_all.shuffle!
          
          f.puts "## Вариант #{var+1}\n\n"
          fa.puts "## Вариант #{var+1}\n\n"
          f.puts "\\vspace*{4mm}\\hrule\n\n\\centerline{(ФИО)}\\vspace*{4mm}\n\n"
          
          @i = 1
        
          2.times do
            f.puts pr_qw(q_min_max_sum)
            fa.puts pr_qw(q_min_max_sum, true)
            @i += 1
          end
          f.puts pr_qw(q_len)
          fa.puts pr_qw(q_len, true)
          @i += 1
          f.puts pr_qw(q_any)
          fa.puts pr_qw(q_any, true)
          @i += 1
          f.puts pr_qw(q_all)
          fa.puts pr_qw(q_all, true)
          @i += 1
          2.times do
            f.puts pr_qw(q_isinstance)
            fa.puts pr_qw(q_isinstance, true)
            @i += 1
          end
          f.puts "\\newpage"
          fa.puts "\\newpage"
        end
      end
    end
    `rake test+fontsize#12pt+nopagenumbers.pdf`
    `rake test_with_answers+fontsize#11pt+nopagenumbers.pdf`
  end
  
end
