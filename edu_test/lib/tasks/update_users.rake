require 'corporate'

desc 'Добавить группу пользователям (единовремнное действие)'
task :add_group2users => :environment do

  User.all.each do |u|
    data = HTTParty.get("https://home.mephi.ru/api/users/#{u.login}.json?token=qz5yk--IQupehsJkwYZG2w&code=EDU_TEST")
    begin
      group = data['students'][0]['groups']['name']
    rescue
      group = ''
    end
    u.update_attribute(:group, group)
  end

end
