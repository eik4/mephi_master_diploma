desc 'Обновить roles у студентов'
task :update_roles => :environment do

  User.where(roles: 0).each do |u|
    u.update_attribute(:roles, 4)
  end
  
end

desc 'Подсчитать кол-во прав. ответов в завершившихся сессиях'
task :update_correct_answers => :environment do

  TestSession.all.each do |ts|
    cnt = 0
    ts.result.each do |_, res|
      cnt += 1 if res
    end
    ts.update_column(:data, ts.data.merge({'correct_answers' => cnt}))
  end
  
end

