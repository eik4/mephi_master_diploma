@add_url = Rails.env.production? ? '../../../tests_data' : '../tests_data'

namespace :load do
  # Загрузка тем (вопросов и ответов)
  #    Вход: в директории (url - параметр Rake-задачи) обрабатываются все
  #          md-файлы с темами/вопросаи/ответами; в этой директории должен
  #          также находиться файл theme.txt, первая строка которого
  #          должна содержать название раздела (множества тем), например:
  #
  #          Набор математики в TeX
  #
  #          при необходимости включения в текст вопросов/ответов
  #          графических изображений все эти изображения должны находиться
  #          в поддректориях директории url и корректные ссылки на них
  #          должны выглядеть, например, так:
  #
  #          ![](images/1.png)
  #
  #
  #
  # Исправить найденную ошибку в тесте, загрузив исправленную версию, можно
  # с помощью этой же задачи. Например,
  #
  # bundle exec rake load:section[first/misc_math]
  #
  # Путь в этой rake-задаче задаётся относительно 'tests_date/sections'
  #
  task :section, [:url] => :environment do |t, args|
    url = "#{@add_url}/sections/#{args[:url]}"
    Dir.chdir(url) do
      name = File.open('theme.txt').gets.strip
      TestBuilder.new(Dir.glob("*.md").sort, name).compile
    end
  end

  # Эта задача во многом аналогична предыдущей
  #
  # Загрузка нового сценария или изменение параметров старого
  task :scenario, [:url] => :environment do |t, args|
    url = "#{@add_url}/scenarios/#{args[:url]}"
    scenario = JSON.parse(File.read(url))

    sc = Scenario.create(
      name: scenario["name"],
      duration: scenario["duration"]
    )
    $stderr.puts "Can't create Scenario #{scenario["name"]}" if sc.nil?

    scenario["themes"].each do |th|
      section = Section.where(name: th["sec_name"], version: nil).first
      $stderr.puts "No such section: #{th["sec_name"]}" if section.nil?

      theme = Theme.where(name: th["th_name"], section: section).first
      $stderr.puts "No such theme: #{th["th_name"]}" if theme.nil?

      ScenarioTheme.create(
        scenario: sc,
        theme: theme,
        questions: [th["questions"], theme.questions.count].min
      )
    end

  end
end
