namespace :db do
  # Переключиться на следующий учебный год
  # (при отсутствии - создать первый)
  
  task :next_study_year => :environment do
    obj = StudyYear.first
    if obj
      obj.update_attributes(start_year: obj.start_year + 1, semester: 1)
    else
      StudyYear.create(start_year: 2018, semester: 1)
    end
  end

  task :next_semester => :environment do
    obj = StudyYear.first
    obj.update_attribute(:semester, 2)
  end

end
