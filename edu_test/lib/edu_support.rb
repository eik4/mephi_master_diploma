class EduSupport
  include HTTParty
  base_uri 'https://edu-support.mephi.ru'
  #base_uri 'http://localhost:3000/'
  headers "Authorization" => "Token token=KW80gbRP23U9ujrxeHmi5A"

  def self.info(login)
    JSON.parse get("/api/users/#{login}.json").body
  end

  def self.groups(code)
    JSON.parse get("/api/groups.json", query: {code: code}).body
  end

  def self.group_info(group, code)
    JSON.parse get("/api/groups/#{group}.json", query: {code: code}).body
  end

  def self.courses(code)
    JSON.parse get("/api/courses.json", query: {term: code}).body
  end

  def self.get_presentation(url)
    buf = ''
    get(url, stream_body: true) do |fragment|
      buf += fragment
    end
    buf
  end
    
end
