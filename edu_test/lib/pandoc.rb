require 'open3'
module Pandoc
  def Pandoc.md2html(str)
    Open3::popen3('pandoc -t html5 --mathjax=https://edu-support.mephi.ru/MathJax/MathJax.js?config=TeX-AMS-MML_SVG-full') do |stdin, stdout, stderr|
      stdin.puts str.gsub(/\$([\.,;:!\?])/){$1+"$"}.gsub(/([^ ]+) (\$[^\$]+\$)/){"<span style=\"display: inline-block;\">"+$1+" "+$2+"</span>"}
      stdin.close
      return stdout.read
    end
  end
end
