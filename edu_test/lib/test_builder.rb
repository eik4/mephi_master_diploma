class TestBuilder
  def initialize(f_names, section_name)
    @section_name = section_name
    @prestate, @curstate, @buf = "", "", ""
    @cur_question = nil
    @answer_variants = []
    @filenames = f_names
    @dir = File.dirname(@filenames[0]) + '/'
  end

  def compile(user = User.first)
    @section = Section.new(user: user, name: @section_name)
    @section.save!
    @filenames.each { |f| parse(f) }
    process_question if @cur_question
  end

  private

  def parse(file)
    f = File.open(file)
    while true
      s = f.gets
      case s

      when /^M\s*$/
        $stderr.puts "Тег M (Macro) не поддерживается (строка #{$.+1} файла #{file})"
        exit 1
        @curstate = 'M'
        error(file) if @prestate != ""
        process

      when /^N\s*\d*\s*$/
        @curstate = 'N'
        error(file) if @prestate == "Q"
        process

      when /^Q\s*$/
        @curstate = 'Q'
        error(file) if @prestate == "" || @prestate == "Q"
        process

      when /^T\s*$/
        @curstate = 'T'
        error(file) if @prestate != "Q" && @prestate != "F" && @prestate != "T"
        process

      when /^T!\s*$/
        @curstate = 'T!'
        error(file) if @prestate != "Q" && @prestate != "F"
        process

      when /^F\s*$/
        @curstate = 'F'
        error(file) if @prestate != "Q" && @prestate != "F" && @prestate != "T" && @prestate != "T!"
        process

      when /^A\s*$/
        @curstate = 'A'
        error(file) if @prestate != "Q" && @prestate != "A"
        process

      when /^AReg\s*$/
        @curstate = 'AReg'
        @file = file
        error(file) if @prestate != "Q"
        process

      when /^S\s*$/
        @curstate = 'S'
        error(file) if @prestate != "Q" && @prestate != "S"
        process

      when /^R\s*$/
        @curstate = 'R'
        error(file) if @prestate != "Q" && @prestate != "R1" && @prestate != "R2"
        process

      when /^R1\s*$/
        @curstate = 'R1'
        error(file) if @prestate != "R" && @prestate != "R2"
        process

      when /^R2\s*$/
        @curstate = 'R2'
        error(file) if @prestate != "R" && @prestate != "R1"
        process

      else
        if s.nil?
          @curstate = ""
          process
          return
        else
          name = s[/\!\[\]\((.*)\)/, 1]
          if name
            p = Picture.create(image: File.new(@dir + name))
            s.gsub!(name, p.image.url)
          end
          @buf << s
        end

      end
    end
  end

  def error(file)
    $stderr.puts "Некорректное использование тега #{@curstate} " +
         (@prestate!="" ? "после тега #{@prestate} " : "в качестве первого ") +
         "в строке #{$.} файла #{file}"
    exit 1
  end

  # Вспомогательный метод создания вопроса (Question)
  def set_question_type_and_code(n)
    @cur_question.question_type_id = n
    @cur_question.code = QuestionType::CODES[n-1]
    @cur_question.save
  end

  # Вход:
  #     @cur_question - полуфабрикат вопроса (тип Question)
  #     @answer_variants - массив ответов
  # Выход:
  #     сохранённая в б/д корректная структура вопроса
  #     "обнулённые" @cur_question и @answer_variants
  def process_question
    # puts "process_question: #{[@cur_question,  @cur_question.body, @answer_variants].inspect}"
    case @answer_variants.first[0]
    when 'A'
      set_question_type_and_code(1)
      Answer.create(question: @cur_question, body: @answer_variants.map{|v| v[1]})
    when 'AReg'
      begin
        eval(@answer_variants.first[1])
      rescue SyntaxError => e
        puts "Ошибка в регулярном выражении в строке #{$.+1} файла #{@file}: #{e.inspect}"
      end
      set_question_type_and_code(2)
      Answer.create(question: @cur_question, body: [@answer_variants.first[1]])
    when 'T', 'T!', 'F'
      set_question_type_and_code(@answer_variants.any?{|a| a.first == 'T!'} ? 3 : 4)
      ids = []
      @answer_variants.each do |a|
        av = AnswerVariant.create(question: @cur_question, body: a[1])
        ids << av.id if a[0] != 'F'
      end
      Answer.create(question: @cur_question, body: ids)
    when 'S'
      set_question_type_and_code(5)
      ids = []
      @answer_variants.each do |a|
        av = AnswerVariant.create(question: @cur_question, body: a[1])
        ids << av.id
      end
      Answer.create(question: @cur_question, body: ids)
    when 'R'
      set_question_type_and_code(6)
      hash = {}
      left = nil
      right = nil
      @answer_variants.each do |a|
        case a[0]
        when 'R'
          hash[left] = right
          left = nil
          right = nil
        when 'R1'
          av = AnswerVariant.create(question: @cur_question, body: a[1], side: 1)
          left = av.id
        when 'R2'
          av = AnswerVariant.create(question: @cur_question, body: a[1], side: 2)
          right = av.id
        end
      end
      hash[left] = right
      hash.delete_if{|k,v| k.blank? && v.nil?}
      Answer.create(question: @cur_question, body: hash)
    end

    @cur_question = nil
    @answer_variants.clear
  end

  def process
    @buf.strip!
    # puts "process: #{[@prestate, @curstate, @buf].inspect}"
    case @prestate
    when 'N'
      @cur_theme = Theme.create(name: @buf, section: @section)
    when 'Q'
      process_question if @cur_question
      #@f_path = @buf[/\!\[\]\((.*)\)/, 1]
      @cur_question = Question.new(theme: @cur_theme, body: @buf)
    else
      #puts "else: #{[@prestate, @buf].inspect}"
      @answer_variants << [@prestate, @buf] unless @prestate.blank?
    end

    @buf = ""
    @prestate = @curstate
  end

end
