class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :theme, index: true, foreign_key: true
      t.text :body
      t.references :question_type, index: true, foreign_key: true
      t.boolean :hidden, default: false
      t.text :code
      
      t.timestamps null: false
    end
  end
end
