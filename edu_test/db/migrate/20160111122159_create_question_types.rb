class CreateQuestionTypes < ActiveRecord::Migration
  def change
    create_table :question_types do |t|
      t.string :name
      t.string :code
      t.boolean :need_variants, default: true
      t.timestamps null: false
    end
  end
end
