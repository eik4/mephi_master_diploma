class CreateRights < ActiveRecord::Migration
  def change
    create_table :rights do |t|
      t.references :obj, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: true
      t.integer :mask

      t.timestamps null: false
    end
  end
end
