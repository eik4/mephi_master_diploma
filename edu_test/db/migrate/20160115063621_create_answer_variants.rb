class CreateAnswerVariants < ActiveRecord::Migration
  def change
    create_table :answer_variants do |t|
      t.string :body
      t.string :side
      t.references :question, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
