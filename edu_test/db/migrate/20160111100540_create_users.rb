class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login
      t.string :surname
      t.string :name
      t.string :patronymic
      t.integer :student_id
      t.integer :staff_id
      t.integer :roles

      t.timestamps null: false
    end
  end
end
