class TestSessions < ActiveRecord::Migration
  def change
    create_table :test_sessions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :scenario, index: true, foreign_key: true
      t.jsonb :data, default: {}
      t.datetime :deadline
      t.timestamps null: false
    end
  end
end
