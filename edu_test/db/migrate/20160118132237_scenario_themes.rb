class ScenarioThemes < ActiveRecord::Migration
  def change
    create_table :scenario_themes do |t|
      t.references :scenario, index: true, foreign_key: true
      t.references :theme, index: true, foreign_key: true
      t.integer :questions
      t.timestamps null: false
    end
  end
end
