class AddVersionToScenarios < ActiveRecord::Migration[5.0]
  def change
    add_column :scenarios, :version, :integer
  end
end
