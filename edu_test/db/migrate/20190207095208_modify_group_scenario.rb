class ModifyGroupScenario < ActiveRecord::Migration[5.0]
  def change
    change_column_default(:scenarios, :groups, [])
  end
end
