class StudyYear < ActiveRecord::Migration[5.0]
  def change
    create_table :study_years do |t|
      t.integer :start_year
      t.integer :semester
      t.timestamps null: false
    end
  end
end
