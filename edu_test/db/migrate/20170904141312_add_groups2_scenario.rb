class AddGroups2Scenario < ActiveRecord::Migration
  def change
    add_column :scenarios, :groups, :string, array: true
  end
end
