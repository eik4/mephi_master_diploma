class AddVersionToSections < ActiveRecord::Migration[5.0]
  def change
    add_column :sections, :version, :integer
  end
end
