# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

QuestionType.create!(code: 'str_open', name: 'Открытый: массив строк', need_variants: false)
QuestionType.create!(code: 'reg_open', name: 'Открытый: регулярное выражение', need_variants: false)
QuestionType.create!(code: 'one_select', name: 'Выбор одного ответа')
QuestionType.create!(code: 'multi_select', name: 'Выбор нескольких вариантов')
QuestionType.create!(code: 'sortable', name: 'Упорядочивание')
QuestionType.create!(code: 'contact', name: 'Сопоставление')

%w(eik001).each do |login|
  # data = HTTParty.get("https://home.mephi.ru/api/users/#{login}.json?token=#{Rails.application.secrets.corporate_tocken}&code=EDU_TEST")
  # staff_id = data['staffs'][0].try(:[], 'staff_id')
  roles = login == 'eik001' ? 3 : 2
  User.create(login: login, surname: 'Егоров', name: 'Иван', patronymic: 'Константинович', roles: roles, staff_id: nil)
end

# %w(intro control1 child misc_math TeX control0 cmd_linux python).each do |name|
#   `bundle exec rake load:section[first/#{name}]`
#   `bundle exec rake load:scenario[first/#{name}]`
  
# end
