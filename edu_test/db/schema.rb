# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190212140858) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answer_variants", force: :cascade do |t|
    t.string   "body"
    t.string   "side"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["question_id"], name: "index_answer_variants_on_question_id", using: :btree
  end

  create_table "answers", force: :cascade do |t|
    t.integer  "question_id"
    t.jsonb    "body",        default: {}
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "question_types", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.boolean  "need_variants", default: true
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "questions", force: :cascade do |t|
    t.integer  "theme_id"
    t.text     "body"
    t.integer  "question_type_id"
    t.boolean  "hidden",           default: false
    t.text     "code"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["question_type_id"], name: "index_questions_on_question_type_id", using: :btree
    t.index ["theme_id"], name: "index_questions_on_theme_id", using: :btree
  end

  create_table "rights", force: :cascade do |t|
    t.string   "obj_type"
    t.integer  "obj_id"
    t.integer  "user_id"
    t.integer  "mask"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["obj_type", "obj_id"], name: "index_rights_on_obj_type_and_obj_id", using: :btree
    t.index ["user_id"], name: "index_rights_on_user_id", using: :btree
  end

  create_table "scenario_themes", force: :cascade do |t|
    t.integer  "scenario_id"
    t.integer  "theme_id"
    t.integer  "questions"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["scenario_id"], name: "index_scenario_themes_on_scenario_id", using: :btree
    t.index ["theme_id"], name: "index_scenario_themes_on_theme_id", using: :btree
  end

  create_table "scenarios", force: :cascade do |t|
    t.string   "name"
    t.integer  "duration"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "groups",     default: [],              array: true
    t.integer  "version"
  end

  create_table "sections", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "version"
    t.index ["user_id"], name: "index_sections_on_user_id", using: :btree
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.string   "cas_ticket"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["cas_ticket"], name: "index_sessions_on_cas_ticket", using: :btree
    t.index ["session_id"], name: "index_sessions_on_session_id", using: :btree
    t.index ["updated_at"], name: "index_sessions_on_updated_at", using: :btree
  end

  create_table "study_years", force: :cascade do |t|
    t.integer  "start_year"
    t.integer  "semester"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "test_sessions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "scenario_id"
    t.jsonb    "data",        default: {}
    t.datetime "deadline"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["scenario_id"], name: "index_test_sessions_on_scenario_id", using: :btree
    t.index ["user_id"], name: "index_test_sessions_on_user_id", using: :btree
  end

  create_table "themes", force: :cascade do |t|
    t.string   "name"
    t.integer  "section_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["section_id"], name: "index_themes_on_section_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.string   "surname"
    t.string   "name"
    t.string   "patronymic"
    t.integer  "student_id"
    t.integer  "staff_id"
    t.integer  "roles"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "group"
  end

  add_foreign_key "answer_variants", "questions"
  add_foreign_key "answers", "questions"
  add_foreign_key "questions", "question_types"
  add_foreign_key "questions", "themes"
  add_foreign_key "rights", "users"
  add_foreign_key "scenario_themes", "scenarios"
  add_foreign_key "scenario_themes", "themes"
  add_foreign_key "sections", "users"
  add_foreign_key "test_sessions", "scenarios"
  add_foreign_key "test_sessions", "users"
  add_foreign_key "themes", "sections"
end
