class Question
  constructor: (form) ->
    @form = $(form)

    type_val = @form.find('#question_question_type_id')
                    .closest('.select.dropdown.selection.ui')
                    .find('.item.active.selected')
                    .data('value')
    if(type_val)
      @type = $('#question_question_type_id').find('option[value=' + type_val + ']').data('type')
    else
      alert("Выбермте тип вопроса.")

ready = ->
  button = $('#add_answer')
  url = button.data('url')
  button.click ->
    q = new Question('.question-form')

    if(q.type)
      $.get url+'?'+'question_type='+q.type, (data, status)->
        console.log(data)
        $('.answer').append(data)
        $.fn.load_nested_fields()
        button.remove()

$(document).ready ready
$(document).on 'page:load', ready
