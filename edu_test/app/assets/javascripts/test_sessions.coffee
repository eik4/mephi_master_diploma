# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

set_count_down = ->
  finishTime = new Date().getTime() + 1000 * parseInt($('#clock').data('duration'))
  $('#clock').countdown finishTime, (event)->
    $(this).html(event.strftime('%H:%M:%S'))
  .on 'finish.countdown', ->
    $('.test-form').submit()
    false

skip_enter = ->
  $(window).keydown (event) ->
    if(event.keyCode == 13)
      event.preventDefault()
      return false
test_tabs = ->
  $('a[data-toggle="tab"]').on 'shown.bs.tab', (ev) ->
    process_test_tabs()
  $(".left-question").on 'click', ->
    console.log $(".questions-list a.active").parent().prev().find("a")
    $(".questions-list a.active").parent().prev().find("a").click()
    false
  $(".right-question").on 'click', ->
    console.log $(".questions-list a.active").parent().next().find("a")
    $(".questions-list a.active").parent().next().find("a").click()
    false
process_test_tabs = ->
  active = $(".questions-list a.active")
  $(".left-question").toggleClass 'invisible', active.hasClass("first-page-link")
  $(".right-question").toggleClass 'invisible', active.hasClass("last-page-link")

ready = ->
  set_count_down()
  skip_enter()
  test_tabs()
  process_test_tabs()

$(document).ready ready
$(document).on 'page:load', ready
