$.fn.dismissive_message = ->
  $('.message .close').on 'click', ->
    $(this).closest('.message').transition('fade')

$.fn.navs_callbacks = ->
  $('a[data-toggle="tab"]').on 'hidden.bs.tab', (e)->
    $(e.target).addClass("text-secondary")
    $(e.target).removeClass('active').parent().removeClass("active")

  $('a[data-toggle="tab"]').on 'shown.bs.tab', (e)->
    $(e.target).parent().addClass("active")
    $(e.target).removeClass("text-secondary")

ready = ->
  $.fn.navs_callbacks()

$(document).ready ready
$(document).on 'page:load', ready
