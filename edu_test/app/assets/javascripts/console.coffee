if !localStorage.getItem "history"
  localStorage.setItem "history", "[]"

history = JSON.parse localStorage.getItem "history"
history_pos = 0

create_input = ->
  input = document.createElement 'div'
  input.classList.add 'console__input'
  input.innerHTML = '<span class=".input__indicator">[psql]</span> > <span class="input__field" contenteditable="true"></span>'
  consoleDom = document.querySelector '.console'
  if !consoleDom
    return
  consoleDom.appendChild input
  input.children[1].focus()
  input.onclick = ->
    input.children[1].focus()
  input.children[1].onkeydown = (e) ->
    if e.key == "Enter"
      history.unshift("")
      if history.length > 100
        history.pop
      localStorage.setItem("history", JSON.stringify history)
      send_query e.target
      history_pos = 0
      return false
    else if e.key == "ArrowUp"
      if (history_pos < history.length - 1)
        history_pos++
        e.target.innerHTML = history[history_pos]
        history[0] = history[history_pos]
        return false
    else if e.key == "ArrowDown"
      if (history_pos > 0)
        history_pos--
        e.target.innerHTML = history[history_pos]
        history[0] = history[history_pos]
        return false
    else
      if e.key.length == 1
        history[0] = "#{e.target.innerHTML}#{e.key}"
        localStorage.setItem("history", JSON.stringify history)
      else if e.key == "Backspace"
        history[0] = "#{e.target.innerHTML}"
        localStorage.setItem("history", JSON.stringify history)

create_output_after = (dom, status = 'success') ->
  dom.setAttribute 'contenteditable', false
  output = document.createElement 'div'
  output.classList.add 'console__output'
  if status == 'error'
    output.classList.add 'console__output_error'
  return output

send_query = (dom) ->
  fetch('http://localhost:8000/psql',
  method: 'POST'
  headers: 'Content-Type': 'application/json'
  body: JSON.stringify(query: dom.innerHTML)).then((response) ->
    response.json().then (json) ->
      consoleDom = document.querySelector '.console'
      output = create_output_after dom, json.status
      consoleDom.appendChild output
      output.innerHTML = json.message
      create_input()
      return
    return
  ).catch (error) ->
    console.log error
    create_input()
    return

console_ready = ->
  create_input()

ready = ->
  console_ready()

$(document).ready ready
$(document).on 'page:load', ready