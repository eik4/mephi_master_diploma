class Theme < ApplicationRecord

  belongs_to :section
  has_many :scenario_themes
  has_many :scenarios, through: :scenario_themes
  has_many :questions, dependent: :destroy

  before_destroy :check_assocs

  validates :name, presence: true

  delegate :name, to: :section, prefix: true

  accepts_nested_attributes_for :questions, reject_if: true

  def questions_count
    questions.count
  end

  private

  def check_assocs
    scenarios.blank? && questions.can_destroy?
  end

end
