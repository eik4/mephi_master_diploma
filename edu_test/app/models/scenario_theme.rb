class ScenarioTheme < ApplicationRecord
  belongs_to :theme
  belongs_to :scenario

  delegate :questions_count, to: :theme, prefix: true

  validates_presence_of :questions, :theme
  #validates :questions, numericality: { less_than_or_equal_to: :theme_questions_count }
  validates :scenario, uniqueness: {scope: :theme}

end
