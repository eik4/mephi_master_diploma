class Question < ApplicationRecord
  belongs_to :theme
  belongs_to :question_type

  has_one :answer, dependent: :destroy
  has_many :answer_variants, dependent: :destroy

  accepts_nested_attributes_for :answer
  accepts_nested_attributes_for :answer_variants, reject_if: :blank?

  delegate :section, to: :theme, prefix: false
  delegate :section_name, to: :theme, prefix: false
  delegate :name, to: :theme, prefix: true
  delegate :need_variants, to: :question_type, allow_nil: true, prefix: false

  validates :body, :question_type_id, :code, presence: true
  # validates_presence_of :answer_variants, if: :need_variants

  before_destroy :can_destroy?
  # TODO: no destroy when has scenarios
  def can_destroy?
    true
  end

  def self.can_destroy?
    true
  end

  def r_answers
    return answer.body if open?
    answer_variants
  end

  def stud_answers
    return [nil] if open?
    if contact?
      # side one always first
      avs1 = answer_variants.group_by(&:side)
      [avs1['1'], avs1['2']].map(&:shuffle).map{ |x| x.map(&:id) }
    else
      answer_variants.shuffle.map(&:id)
    end
  end

  def open?
    %w(str_open reg_open).include?(code)
  end

  def str_open?
    code == 'str_open'
  end

  def reg_open?
    code == 'reg_open'
  end

  def multi_select?
    code == 'multi_select'
  end

  def contact?
    code == 'contact'
  end

  def sortable?
    code == 'sortable'
  end
end
