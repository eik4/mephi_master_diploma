class StudyYear < ApplicationRecord

  def self.first_semester
    StudyYear.first.start_year.to_s + '1'
  end

  def self.second_semester
    StudyYear.first.start_year.to_s + '2'
  end

  def self.current_semester
    StudyYear.first.start_year.to_s + StudyYear.first.semester.to_s
  end

  def self.current_year
    year = StudyYear.first.start_year
    "#{year}/#{year+1}"
  end

  def self.schedule_year
    StudyYear.first.start_year
  end

end
