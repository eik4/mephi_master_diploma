class Section < ApplicationRecord
  include Active

  belongs_to :user
  has_many :themes, dependent: :destroy

  accepts_nested_attributes_for :themes, reject_if: :blank?

  validates :name, :user, presence: true

  scope :active, ->  { where(version: nil) }

  before_create :check_same_name

  def old
    Section.where(name: name).order(created_at: :desc)
  end

  def full_name
    "#{name} #{'(v' + version.to_s + ')' if version}"
  end
end
