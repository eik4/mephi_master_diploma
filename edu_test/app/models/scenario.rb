class Scenario < ApplicationRecord
  include Active
  has_many :scenario_themes, dependent: :destroy
  has_many :themes, through: :scenario_themes
  has_many :test_sessions, dependent: :nullify

  before_create :check_same_name

  accepts_nested_attributes_for :scenario_themes, reject_if: :blank?

  validate :questions_in_themes

  scope :active, ->  { where(version: nil) }

  def questions
    scenario_themes.includes(:theme).map do |st|
      st.theme.questions.order('random()').limit(st.questions)
    end.flatten.shuffle
  end

  def old
    Scenario.where(name: name).order(created_at: :desc)
  end

  def full_name
    "#{name} #{'(v' + version.to_s + ')' if version}"
  end

  #Разрешено ли пользователю тестироваться
  def allow?(u)
    return false unless u
    return true if u.tutor? || u.admin?
    info = EduSupport.info(u.login)
    info && (info['courses'].any?{|c| groups.present? && groups.include?(c.dig('course', 'group', 'name'))} || info['students'].any?{|g| groups.present? && groups.include?(g['name'])})
  end

  private

  def questions_in_themes
    errors.add(:themes, 'Количество вопросов в теме некорректно.') unless scenario_themes.map(&:validate).inject(:&).inspect
  end

end
