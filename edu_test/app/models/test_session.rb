# User test data
class TestSession < ApplicationRecord
  belongs_to :user
  belongs_to :scenario

  attr_accessor :page_size
  attr_accessor :group
  attr_accessor :before
  attr_accessor :after

  scope :full, -> { includes(:scenario).references(:scenario).order('scenarios.name, test_sessions.updated_at') }
  scope :search, ->(q) { full.where('scenarios.name ilike ? ', '%' + q + '%') if q.present? }

  delegate :questions, to: :scenario, prefix: true
  delegate :name,      to: :scenario, prefix: true

  after_create :set_questions_and_answers
  before_save :count_correct_answers!

  def questions
    set_questions_and_answers if data.blank?
    Question.find(data['questions']).sort { |x, y| data['questions'].index(x.id) <=> data['questions'].index(y.id) }
  end

  def expired?
    deadline < Time.now
  end

  # Order of AnswerVariant must be same in not expired session.
  def stud_answers(q)
    code_answers = data['code_answer'][q.id.to_s]
    return [nil] if q.open?
    ret_val = []
    if q.contact?
      avs = [AnswerVariant.find(code_answers[0]).sort_by{|x| code_answers[0].index(x.id)},
             AnswerVariant.find(code_answers[1]).sort_by{|x| code_answers[1].index(x.id)}]
      ret_val = avs.transpose
    else
      avs = q.answer_variants
      ret_val = avs.sort_by { |x| code_answers.index(x.id) }
    end
    ret_val
  end

  def convert_answer
    a_data = data['params']
    decode = {}
    data['questions'].each_with_index do |q, i|
      question = Question.find(q)
      decode[q.to_s] = nil
      unless a_data.blank?
        a_data_i = a_data[i.to_s]
        decode[q.to_s] = send("decode_#{question.code}", a_data_i, data['code_answer'][q.to_s]) if a_data_i.present? && (!%w(contact sortable).include?(question.code) || a_data_i.values.any?{|v| !v.blank?})
      end
    end
    data['answer'] = decode
    save!
  end

  def result
    r = {}
    questions.each do |q|
      s_qid = q.id.to_s
      next if data['answer'].blank?
      if data['answer'][s_qid].blank?
        r[q] = nil
      elsif q.str_open?
        r[q] = q.answer.body.include?(data['answer'][s_qid])
      elsif q.reg_open?
        r[q] = (data['answer'][s_qid] =~ eval(q.answer.body[0])) || false
      elsif q.multi_select?
        r[q] = data['answer'][s_qid].sort == q.answer.body.sort
      elsif q.sortable?
        r[q] = data['answer'][s_qid] == q.answer.body.map{|t| [t]}
      elsif q.contact?
        r[q] = data['answer'][s_qid] == q.answer.body.delete_if { |k, _| k.blank? }
        r[q] = nil if data['answer'][s_qid].blank?
      else
        r[q] = data['answer'][s_qid] == q.answer.body
      end
    end
    r
  end

  def correct_answers
    data['correct_answers']
  end

  private

  def count_correct_answers!
    cnt = 0
    result.each do |_, res|
      cnt += 1 if res
    end
    self.data = self.data.merge({'correct_answers' => cnt})
    cnt
  end

  def decode_sortable(a_data, q_data)
    decode_q = Array.new(a_data.size){|_| []}
    a_data.each do |key, val|
      decode_q[val.to_i - 1] << q_data[key.to_i]
    end
    decode_q
  end

  def decode_str_open(a_data, _)
    a_data
  end

  def decode_reg_open(a_data, _)
    a_data
  end

  def decode_one_select(a_data, q_data)
    decode_q = []
    a_data.each { |index| decode_q << q_data[index.to_i] }
    decode_q
  end

  alias decode_multi_select decode_one_select

  def decode_contact(a_data, q_data)
    decode_q = {}
    a_data.each do |a_num, c_num|
      a = q_data[0][a_num.to_i].to_s
      decode_q[a] ||= []
      decode_q[a] << (c_num.blank? ? nil : q_data[1][c_num.to_i - 1])
      decode_q[a].compact!
    end
    decode_q.each  do |k, v|
      decode_q[k] = v[0] if v.size == 1
      decode_q[k] = nil if v.size == 0
    end
    decode_q
  end

  def set_questions_and_answers
    self.data ||= {}
    self.data['questions'] = scenario.questions.map(&:id)
    self.data['code_answer'] = {}
    self.data['questions'].each do |q|
      self.data['code_answer'][q.to_s] = Question.find(q).stud_answers
    end
    save!
  end

end
