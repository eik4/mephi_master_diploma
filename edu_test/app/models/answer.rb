class Answer < ApplicationRecord
  belongs_to :question

  attr_accessor :text

  # validates :body, presence: true

  # Структура поля body для объекта Answer
  #   Тип теста   body_type    пример               комментарий
  #      1         Аrray       ['a','b']       массив строк
  #      2         Arary       [/a*[bc]/]      массив из одного RegExp
  #      3         Array       [123]           идентификатор объекта variants
  #      4         Array       [123,124]       массив идентификаторов
  #      5         Array       [23,24,25,17]   массив идентификаторов variants
  #      6         Hash        {''=>[99],      хэш соответствий
  #                             [45]=>[87],
  #                              [47]=>nil}

  def type
    question.code
  end
end
