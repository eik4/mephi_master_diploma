class QuestionType < ApplicationRecord
  has_many :questions
  CODES = %w(str_open reg_open one_select multi_select sortable contact).freeze

  validates :code, uniqueness: true

  CODES.each do |c_name|
    scope c_name.to_sym, -> { where(code: c_name).first }
  end
end
