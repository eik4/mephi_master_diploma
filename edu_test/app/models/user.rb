class User < ApplicationRecord
  ROLES = %w(admin tutor student).freeze

  validates :roles, presence: true, inclusion: { in: 0...2**ROLES.size }

  scope :search, ->(q){ where('surname ilike ? OR name ilike ? or patronymic ilike ? or login ilike ?', *['%'+q+'%']*4) if q.present? }

  has_many :test_sessions

  def admin?
    roles && roles & 1 > 0
  end

  def tutor?
    roles && roles & 2 > 0
  end

  def student?
    roles && roles & 4 > 0
  end

  def only_student?
    student? && !(tutor? || admin?)
  end

  def self.corporate(login)
    user = User.where(login: login).first
    unless user
      data = HTTParty.get("https://home.mephi.ru/api/users/#{login}.json?token=#{Rails.application.secrets.corporate_tocken}&code=EDU_TEST")
      roles = 0
      student_id = data['students'][0].try(:[], 'student_id')
      staff_id = data['staffs'][0].try(:[], 'staff_id')
      tutor = data['staffs'].map { |s| s.try(:[], 'contracts') }.flatten.count { |c| c.try(:[], 'category_name') == 'ППС' } > 0
      roles |=  4 if student_id
      roles |=  2 if tutor
      # Полезно!
      roles |= 4 if roles == 0

      begin
        group = data['students'][0]['groups']['name']
      rescue
        group = ''
      end

      user = User.create(login: login, surname: data['surname'], name: data['name'], patronymic: data['patronymic'], roles: roles, group: group, student_id: student_id, staff_id: staff_id)
    end
    user
  end

  def full_name
    # raise login.inspect
    [surname, name, patronymic].reject(&:blank?).join(' ') + " (#{login})"
  end

  def short_name
    [name, patronymic].reject(&:blank?).map{|t| "#{t[0]}."}.join +
      " #{surname}" +  " (#{[login, group].delete_if(&:blank?).join(', ')})"
  end

end
