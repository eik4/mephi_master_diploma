module Active
  extend ActiveSupport::Concern
  def check_same_name
    prev = self.class.where(name: name).order(:created_at)
    if prev.present?
      prev.last.version = prev.size
      prev.last.save
    end
  end

end
