class Group < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true

  GROUPS = ["Б15-103 (1)", "Б15-103 (2)", "Б16-501 (1)", "Б16-501 (2)", "Б16-504 (1)", "Б16-504 (2)", "Б16-511", "Б16-564", "Б16-701", "М17-117", "М18-102", "М18-117", "М18-165", "М18-167", "С16-562"]

end
