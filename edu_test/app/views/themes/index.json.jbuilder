json.array!(@themes) do |theme|
  json.extract! theme, :id, :name, :user_id, :section_id
  json.url theme_url(theme, format: :json)
end
