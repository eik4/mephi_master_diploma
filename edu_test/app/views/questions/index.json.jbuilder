json.array!(@questions) do |question|
  json.extract! question, :id, :user_id, :theme_id, :body, :question_type_id
  json.url question_url(question, format: :json)
end
