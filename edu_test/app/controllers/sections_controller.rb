class SectionsController < ApplicationController
  before_action :admin_user, except: [:index, :show]
  before_action :tutor_user, only: [:index, :show]
  before_action :set_section, only: [:show, :edit, :update, :destroy]
  # GET /sections
  # GET /sections.json
  def index
    redirect_to test_sessions_path if @current_user.only_student?
    @sections = Section.all.order(updated_at: :desc, name: :asc).active.page(params[:page])
  end

  # GET /sections/1
  # GET /sections/1.json
  def show
  end

=begin
  # GET /sections/new
  def new
    @section = Section.new
  end

  # GET /sections/1/edit
  def edit
  end

  # POST /sections
  # POST /sections.json
  def create
    @section = Section.new(section_params)
    @section.user = @current_user
    respond_to do |format|
      if @section.save
        format.html { redirect_to @section, notice: 'Section was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to @section, notice: 'Section was successfully updated.' }
        format.json { render :show, status: :ok, location: @section }
      else
        format.html { render :edit }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to sections_url, notice: 'Section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def auth_ivar
    @section
  end
=end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_section
    @section = Section.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def section_params
    params.require(:section).permit(:name, :user_id, themes_attributes: [:name])
  end
end
