require 'edu_support'
require 'students'
# Controller contains operations with user test sessions
class TestSessionsController < ApplicationController
  DELTA = 10

  layout 'student', except: [:index, :show, :pass]

  before_action :set_test_session, only: [:pass, :pass_part]
  before_action :validate_user_test_session, only: [:pass, :pass_part, :new]

  # OK
  def index
    if @current_user.only_student?
      @test_sessions = TestSession.includes(:user, :scenario).where(user_id: @current_user.id).order("updated_at DESC").page(params[:page]).per(25)
      render :stud_index and return
    end
    page_size = params[:page_size] || 25
    cond = []

    cond << "(scenario_id in (#{params['scenario'].map{|q| q.to_i}.join(',')}))" unless params['scenario'].blank?

    unless params['user'].blank?
      user = User.where(login: params['user']).first
      id = user.nil? ? 0 : user.id
      cond << "(user_id = #{id})"
    end

    unless params[:group].blank?
      begin
        #raise EduSupport.group_info(URI::encode(params[:group]), StudyYear.current_semester)['students'].inspect
        ids = params[:group].upcase.split.map do |g|
          EduSupport.group_info(URI::encode(g), StudyYear.current_semester)['students'].map{|q| User.where(login: q['login']).first.try(:id)}.compact
        end.flatten
      rescue
        ids = []
      end
      ids = [0] if ids.size == 0
      cond << "(user_id in (#{ids.join(',')}))"
    end

    unless params[:before].blank?
      begin
        t = Time.parse(params[:before])
        cond << "(updated_at <= '#{t.utc}')"
      rescue
      end
    end

    unless params[:after].blank?
      begin
        t = Time.parse(params[:after])
        cond << "(updated_at >= '#{t.utc}')"
      rescue
      end
    end

    cond = cond.join(' and ')
    @test_sessions = TestSession.includes(:user, :scenario).where(cond).order("updated_at DESC").page(params[:page]).per(params[:page_size])
    @scenarios = Scenario.all.order(updated_at: :desc, name: :asc)
    @groups = Group.all
  end

  # Попытка начать тестирование (корректная или нет)
  def new
    begin
      scenario = Scenario.find(params[:scenario_id])
      unless scenario.version.nil?
        redirect_to :scenario_id => Scenario.where(name: scenario.name, version: nil).first.id and return
      end
    rescue
      redirect_to :scenario_id => Scenario.first.id and return
    end

    unless @current_user.tutor?
      unless scenario.allow?(@current_user)
        render_error('Тестирование запрещено') and return
      end
    end

    @test_session = TestSession.where(id: session[:test_session]).first ||
                    TestSession.where(user_id: @current_user.id, scenario_id: scenario.id).last
    if @test_session.blank? || @test_session.expired?
      @test_session = TestSession.create!(user: @current_user, scenario_id: scenario.id, deadline: Time.now.advance(minutes: scenario.duration))
    elsif @test_session.scenario_id != scenario.id
      redirect_to :scenario_id => @test_session.scenario_id and return
    end
    session[:test_session] = @test_session.id
  end

  # Завершение тестирования (корректного или нет)
  def pass
    if @test_session.blank?
      render_error "Тестирование уже было завершено" and return
    end
    begin
      if Time.now < @test_session.deadline.advance(seconds: DELTA)
        @test_session.deadline = Time.now
        @test_session.save # Сохраняем, так как следующая строка может
                           # и не выполниться (в случае "подлога")
        # Действия ниже могут оказаться относящимися к иной сессии
        # (если человек совершает некорректные операции)
        # В этом случае, скорее всего, конвертация не пройдёт!
        @test_session.data['params'] = params[:answer]
        @test_session.convert_answer
      end
    rescue
    end
    session.delete(:test_session)
  end

  # Переход к иному вопросу (в корректном тестировании или нет)
  def pass_part
    begin
      if Time.now < @test_session.deadline.advance(seconds: DELTA)
        @test_session.data['params'] = params[:answer]
        @test_session.convert_answer
      end
      ids = []
      unless @test_session.data['answer'].nil?
        @test_session.data['questions'].each_with_index do |q, i|
          ids << i.to_s if @test_session.data['answer'][q.to_s]
        end
      end
      render json: { success: true, data: { tabs: ids } }
    rescue
      render json: { error: true }
    end
  end

  # Показ результатов при наличии прав и корректности запроса
  def show
    begin
      @test_session = TestSession.find(params[:id])
    rescue
      redirect_to root_path
    end
    validate_user_test_session unless @current_user.tutor? || @current_user.admin?
  end

  private

  def set_test_session
    @test_session = TestSession.where(id: session[:test_session]).first
  end

  def validate_user_test_session
    if @test_session && @current_user != @test_session.user
      redirect_to root_path
    end
  end
end
