class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :check_authentication
  before_action :load_current_user
  before_filter :set_cache_buster

  protect_from_forgery with: :exception

  helper_method :current_login

  def check_authentication
    if session.blank? || session['cas'].blank? || session['cas']['user'].blank? ||
       (request.get? && !request.xhr? && (session['cas']['last_validated_at'].blank? || session['cas']['last_validated_at'] < 15.minutes.ago))
      render text: 'Требуется авторизация', status: 401
    end
  end

  def index
    p 'application index'
    if @current_user.only_student?
      redirect_to test_sessions_path
    else
      redirect_to sections_path
    end
  end

  private

  def render_error(error = t('helpers.errors.no_rights'), options = {})
    @error = error
    status = options[:status] || 403
    respond_to do |format|
      format.html { render 'error', layout: false, status: status }
      format.js { render js: %(alert("#{@error}")), status: status }
      format.json { render json: { error: @error }, status: status }
      format.xml { render xml: { error: @error }, status: status }
    end
  end

  def admin_user
    render_error(t('error.no_rights')) unless @current_user.admin?
  end

  def tutor_user
    render_error(t('error.no_rights')) unless @current_user.admin? || @current_user.tutor?
  end

  def load_current_user
    @current_user = current_user
    render_error("Пользователь не найден", status: 404) unless @current_user
  end

  def current_user
    return nil if current_login.blank?
    User.corporate(current_login)
  end

  def current_login
    session[:zombie] || session['cas'] && session['cas']['user']
  end

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
end
