class ScenariosController < ApplicationController
  before_action :admin_user, except: [:index, :show, :edit, :update]
  before_action :tutor_user, only: [:index, :show, :edit, :update]
  before_action :set_scenario, only: [:show, :edit, :update, :destroy, :test, :test_sessions]
  before_action :set_groups, only: [:show, :edit, :new]

  # GET /scenarios
  # GET /scenarios.json
  def index
    @scenarios = Scenario.all.order(updated_at: :desc, name: :asc).active.page(params[:page])
  end

  # GET /scenarios/1
  # GET /scenarios/1.json
  def show
  end

  # GET /scenarios/new
  def new
    @scenario = Scenario.find(params[:scenario_id])
    @scenario.name = @scenario.name += " (клон #{rand(1_000_000)})"
=begin
    @scenario = Scenario.create(
      name: scenario.name += " (клон #{rand(1_000_000)})",
      duration: scenario.duration,
      groups: []
    )
    scenario.scenario_themes.each do |th|
      ScenarioTheme.create(scenario_id: @scenario.id, theme_id: th.theme.id, questions: th.questions)
    end
    @scenario.reload
=end
  end

  # GET /scenarios/1/edit
  def edit
    case params['groups']
    when 'all'
      @scenario.groups = @groups
    when 'none'
      @scenario.groups = []
    else
      @scenario.save
      render :edit and return
    end
    @scenario.save
    render :show
  end

=begin
  # POST /scenarios
  # POST /scenarios.json
  def create
    @scenario = Scenario.new(scenario_params)
    respond_to do |format|
      if @scenario.save
        format.html { redirect_to scenarios_path, notice: 'Scenario was successfully updated.' }
        format.json { render :show, status: :created, location: @scenario }
      else
        format.html { render :new }
        format.json { render json: @scenario.errors, status: :unprocessable_entity }
      end
    end
  end
=end
  def update
    if params['scenario']['name']
      scenario_new = Scenario.create(
        name: params['scenario']['name'],
        duration: params['scenario']['duration'],
        groups: []
      )
      @scenario.scenario_themes.each do |th|
        ScenarioTheme.create(scenario_id: scenario_new.id, theme_id: th.theme.id, questions: th.questions)
      end
      redirect_to scenario_new, notice: 'Сценарий создан'
    else
      @scenario.groups = params.dig(:scenario, :groups).reject{|q| q.blank?}
      if @scenario.save
        redirect_to @scenario, notice: 'Сценарий изменен'
      else
        render :show
      end
    end
  end
=begin
  # DELETE /scenarios/1
  # DELETE /scenarios/1.json
  def destroy
    @scenario.destroy
    respond_to do |format|
      format.html { redirect_to scenarios_url, notice: 'Scenario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
=end

  def test_sessions
    @test_sessions = @scenario.test_sessions.includes(:user).references(:user).order('users.surname')
  end

  private

  def set_groups
    @groups = EduSupport.courses(StudyYear.current_semester).map do |t|
      t['groups'].map{|q| q['group'].nil? ? q['name'] : (q['group'] + (q['name'].nil? ? '' : " (#{q['name']})"))}
    end.flatten.sort
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_scenario
    @scenario = Scenario.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def scenario_params
    params.require(:scenario).permit(:name, :duration, :groups, scenario_themes_attributes: [:id, :theme_id, :questions, :_destroy])
  end
end
