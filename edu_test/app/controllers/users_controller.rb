class UsersController < ApplicationController
  before_action :admin_user, except: [:test_sessions, :reset_current_user, :index]
  before_action :tutor_user, only: :index

  def edit_current_user
  end

  def reset_current_user
    session.delete(:zombie)
    redirect_to root_path
  end

  def update_current_user
    @user = User.corporate(params[:login])
    if @user
      session[:zombie] = @user.login
      redirect_to root_path
    else
      flash[:danger] = "Пользователь не найден"
      render :edit_current_user
    end
  end

  def index
    @users = User.search(params[:q]).order(:surname, :name, :patronymic)
  end

  def test_sessions
    if params[:id].to_i != @current_user.id && @current_user.only_student?
      redirect_to :id => @current_user.id and return
    end
    @user = User.includes(test_sessions: :scenario).references(test_sessions: :scenario).order('scenarios.name, test_sessions.updated_at').find(params[:id])
    @user.test_sessions.search(params[:q]).page(params[:page])
    @ts = @user.test_sessions.search(params[:q]).page(params[:page])
  end

end
