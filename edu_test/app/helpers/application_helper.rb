module ApplicationHelper
  def current_fio
    @current_user.full_name
  rescue
    nil
  end

  def collapse_tag
    button_tag(type: 'button', class: 'navbar-toggle collapsed', data: { toggle: 'collapse', target: '#collapse-menu' }) do
      fa_icon('bars')
    end
  end

  def title(controller, action = nil)
    controller, action = controller.split('#') if action.nil?
    t action, scope: [:helpers, :titles, controller]
  end

  def divider_tag
    content_tag(:span, '/', class:  'divider')
  end

  def markdown(text, delete_p = false)
    res = Pandoc.md2html(text)
    delete_p && res =~ /\A<p>/ ? res[3..-6] : res
  end

  def copyright_years
    year1 = 2017
    year2 = Date.today.year
    res="© #{t('app.company.short_name')}, "
    res += year1 == year2 ? "#{year1}" : "#{year1}–#{year2}"
    content_tag(:span, res)
  end

  def default_title_tag(title = nil)
    content_for(:title){title || t('.title')}
  end

  def error_messages_for(object)
    render 'error_messages_for', msgs: object.errors.full_messages
  end
end
