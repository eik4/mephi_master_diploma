module TestSessionsHelper
  def result_class(r)
    return 'text-success fa fa-check' if r
    return 'text-warning fa fa-question' if r.nil?
    'text-danger fa fa-remove'
  end

  def result_text(r)
    return 'сдано' if r
    return 'нет ответа' if r.nil?
    'не сдано'
  end

  def active_class(index, options = {})
    res = []
    res.push 'active' if index == 0
    if options[:include_nav]
       if index == 0
         res.push 'first-page-link'
       elsif index + 1 == options[:size]
         res.push 'last-page-link'
       end
    end
    res
  end

  def duration_session(ts)
    (ts.deadline - Time.now()).to_i
  end

end
