module AnswerVariantsHelper
  def variant_color(variant, answer)
    answer.try(:include?, variant.id) ? 'text-success' : 'text-danger'
  end
end
