module SectionsHelper
  def sections_breadcrumb(options = {})
    link_to(title(:sections, :index), sections_path, class: "breadcrumb-item #{'active' if options[:active]} section")
  end
end
