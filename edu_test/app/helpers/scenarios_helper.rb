module ScenariosHelper
  def scenarios_breadcrumb(options = {})
    link_to(title(:scenarios, :index), scenarios_path, class: "#{'active' if options[:active]} section") + divider_tag
  end

end
