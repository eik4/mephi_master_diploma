module UsersHelper
  def users_breadcrumb(options = {})
    link_to(title(:users, :index), users_path(q: params[:q]), class: "#{'active' if options[:active]} section") + divider_tag
  end
end
