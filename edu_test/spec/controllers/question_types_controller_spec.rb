require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe QuestionTypesController, type: :controller do
  before(:all) do
    @thing = FactoryGirl.create(:admin)
    @thing = FactoryGirl.create(:user)
  end
  # This should return the minimal set of attributes required to create a valid
  # QuestionType. As you add validations to QuestionType, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {{
    code: QuestionType::CODES.sample,
    name: 'zzzz'
  }}

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # QuestionTypesController. Be sure to keep this updated too.
  let(:valid_session) { {cas: {'user' => 'admin', 'last_validated_at' => Time.now}} }

  describe "GET #index" do
    it "assigns all question_types as @question_types" do
      question_type = QuestionType.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:question_types)).to eq([question_type])
    end
  end
end
