FactoryGirl.define do
  factory :user do
    surname "Doe"
    name "John"
    login 'dj'
    roles 0
  end

  # This will use the User class (Admin would have been guessed)
  factory :admin, class: User do
    surname "Admin"
    name  "User"
    login 'admin'
    roles 7
  end

  factory :section do
    name "Section 1"
  end

  factory :theme do
    name "Theme_1_Section_1"
    section
  end

  factory :question_type do
    name 'QQQ'
    code QuestionType::CODES.sample
  end

  factory :question do
    body 'question_'
    theme
    question_type
  end
end
