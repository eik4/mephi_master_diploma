require 'rails_helper'

RSpec.describe "scenarios/edit", type: :view do
  before(:each) do
    @scenario = assign(:scenario, Scenario.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit scenario form" do
    render

    assert_select "form[action=?][method=?]", scenario_path(@scenario), "post" do

      assert_select "input#scenario_name[name=?]", "scenario[name]"
    end
  end
end
