require 'rails_helper'

RSpec.describe "scenarios/new", type: :view do
  before(:each) do
    assign(:scenario, Scenario.new(
      :name => "MyString"
    ))
  end

  it "renders new scenario form" do
    render

    assert_select "form[action=?][method=?]", scenarios_path, "post" do

      assert_select "input#scenario_name[name=?]", "scenario[name]"
    end
  end
end
