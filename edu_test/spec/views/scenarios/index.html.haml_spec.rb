require 'rails_helper'

RSpec.describe "scenarios/index", type: :view do
  before(:each) do
    assign(:scenarios, [
      Scenario.create!(
        :name => "Name"
      ),
      Scenario.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of scenarios" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
