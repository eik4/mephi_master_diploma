require 'rails_helper'

RSpec.describe "scenarios/show", type: :view do
  before(:each) do
    @scenario = assign(:scenario, Scenario.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
