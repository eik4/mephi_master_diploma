require 'rails_helper'

RSpec.describe "questions/new", type: :view do
  before(:each) do
    assign(:question, Question.new(
      :user => nil,
      :theme => nil,
      :body => "MyText",
      :question_type => nil
    ))
  end

  it "renders new question form" do
    render

    assert_select "form[action=?][method=?]", questions_path, "post" do

      assert_select "input#question_user_id[name=?]", "question[user_id]"

      assert_select "input#question_theme_id[name=?]", "question[theme_id]"

      assert_select "textarea#question_body[name=?]", "question[body]"

      assert_select "input#question_question_type_id[name=?]", "question[question_type_id]"
    end
  end
end
