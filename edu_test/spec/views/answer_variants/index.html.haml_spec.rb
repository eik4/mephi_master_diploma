require 'rails_helper'

RSpec.describe "answer_variants/index", type: :view do
  before(:each) do
    assign(:answer_variants, [
      AnswerVariant.create!(
        :body => "Body",
        :alignment => "Alignment"
      ),
      AnswerVariant.create!(
        :body => "Body",
        :alignment => "Alignment"
      )
    ])
  end

  it "renders a list of answer_variants" do
    render
    assert_select "tr>td", :text => "Body".to_s, :count => 2
    assert_select "tr>td", :text => "Alignment".to_s, :count => 2
  end
end
