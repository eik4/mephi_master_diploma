require 'rails_helper'

RSpec.describe "answer_variants/show", type: :view do
  before(:each) do
    @answer_variant = assign(:answer_variant, AnswerVariant.create!(
      :body => "Body",
      :alignment => "Alignment"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Body/)
    expect(rendered).to match(/Alignment/)
  end
end
