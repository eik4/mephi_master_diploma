require 'rails_helper'

RSpec.describe "answer_variants/edit", type: :view do
  before(:each) do
    @answer_variant = assign(:answer_variant, AnswerVariant.create!(
      :body => "MyString",
      :alignment => "MyString"
    ))
  end

  it "renders the edit answer_variant form" do
    render

    assert_select "form[action=?][method=?]", answer_variant_path(@answer_variant), "post" do

      assert_select "input#answer_variant_body[name=?]", "answer_variant[body]"

      assert_select "input#answer_variant_alignment[name=?]", "answer_variant[alignment]"
    end
  end
end
