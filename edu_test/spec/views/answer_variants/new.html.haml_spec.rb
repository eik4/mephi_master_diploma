require 'rails_helper'

RSpec.describe "answer_variants/new", type: :view do
  before(:each) do
    assign(:answer_variant, AnswerVariant.new(
      :body => "MyString",
      :alignment => "MyString"
    ))
  end

  it "renders new answer_variant form" do
    render

    assert_select "form[action=?][method=?]", answer_variants_path, "post" do

      assert_select "input#answer_variant_body[name=?]", "answer_variant[body]"

      assert_select "input#answer_variant_alignment[name=?]", "answer_variant[alignment]"
    end
  end
end
