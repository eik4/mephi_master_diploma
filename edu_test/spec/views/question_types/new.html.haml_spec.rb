require 'rails_helper'

RSpec.describe "question_types/new", type: :view do
  before(:each) do
    assign(:question_type, QuestionType.new(
      :name => "MyString",
      :klass => "MyString"
    ))
  end

  it "renders new question_type form" do
    render

    assert_select "form[action=?][method=?]", question_types_path, "post" do

      assert_select "input#question_type_name[name=?]", "question_type[name]"

      assert_select "input#question_type_klass[name=?]", "question_type[klass]"
    end
  end
end
